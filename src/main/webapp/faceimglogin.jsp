<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>人脸认证</title>
<link rel="Shortcut Icon" href="/img/Gnosisicon.png">
<link href="/user/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/user/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="/css/getpass.css" rel="stylesheet">
<link href="/css/component.css" rel="stylesheet" />
<script src="/js/jquery-1.11.2.min.js"></script>
<script src="/js/classie.js"></script>
<script src="/js/modalEffects.js"></script>
</head>
<body>

	<div id="overlay" class="overlay"></div>
	<div class="container2">
		<div class="container">
			<img src="/img/LOGO.png" /> <span>丨人脸认证</span>
			<div class="login" style="text-align: right; font-size: 14px;">
				<a href="/login.jsp" class="load">登录</a> <a href="register.jsp" class='reg'>注册</a>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row clearfix">
			<div class="col-md-4 column">
				<canvas id="canvas2" height="280px"></canvas>
			</div>
			<div class="col-md-4 column">
				<h2 align="center">人脸识别窗口</h2>
				<br />
				<h3 align="center">请目视前方</h3>
				<br />
				<video height="280px" autoplay="autoplay"></video>
				<input class="btn btn-danger" style="margin: 20px 130px" type="button" title="拍照" value="拍照/重拍" onclick="getPhoto();" /> <br /> <br /> <img id="img" alt="" src="">
			</div>
			<div class="col-md-4 column">
				<canvas id="canvas1" height="280px" width="374px" style="display: none"></canvas>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			getMedia();

		});

		var video = document.querySelector('video');
		var audio, audioType;

		var canvas1 = document.getElementById('canvas1');
		var context1 = canvas1.getContext('2d');

		var canvas2 = document.getElementById('canvas2');
		var context2 = canvas2.getContext('2d');

		navigator.getUserMedia = navigator.getUserMedia
				|| navigator.webkitGetUserMedia || navigator.mozGetUserMedia
				|| navigator.msGetUserMedia;
		window.URL = window.URL || window.webkitURL || window.mozURL
				|| window.msURL;

		var exArray = []; //存储设备源ID  
		MediaStreamTrack.getSources(function(sourceInfos) {
			for (var i = 0; i != sourceInfos.length; ++i) {
				var sourceInfo = sourceInfos[i];
				//这里会遍历audio,video，所以要加以区分  
				if (sourceInfo.kind === 'video') {
					exArray.push(sourceInfo.id);
				}
			}
		});

		function getMedia() {
			if (navigator.getUserMedia) {
				navigator.getUserMedia({
					'video' : {
						'optional' : [ {
							'sourceId' : exArray[1]
						//0为前置摄像头，1为后置  
						} ]
					},
					'audio' : true
				}, successFunc, errorFunc); //success是获取成功的回调函数  
			} else {
				alert('Native device media streaming (getUserMedia) not supported in this browser.');
			}
		}

		function successFunc(stream) {
			//alert('Succeed to get media!');  
			if (video.mozSrcObject !== undefined) {
				//Firefox中，video.mozSrcObject最初为null，而不是未定义的，我们可以靠这个来检测Firefox的支持  
				video.mozSrcObject = stream;
			} else {
				video.src = window.URL && window.URL.createObjectURL(stream)
						|| stream;
			}

			//video.play();  

			// 音频  
			audio = new Audio();
			audioType = getAudioType(audio);
			if (audioType) {
				audio.src = 'polaroid.' + audioType;
				audio.play();
			}
		}
		function errorFunc(e) {
			alert('Error！' + e);
		}

		// 将视频帧绘制到Canvas对象上,Canvas每60ms切换帧，形成肉眼视频效果  
		function drawVideoAtCanvas(video, context) {
			window.setInterval(function() {
				context.drawImage(video, 0, 0, 374, 280);
			}, 60);
		}

		//获取音频格式  
		function getAudioType(element) {
			if (element.canPlayType) {
				if (element.canPlayType('audio/mp4; codecs="mp4a.40.5"') !== '') {
					return ('aac');
				} else if (element.canPlayType('audio/ogg; codecs="vorbis"') !== '') {
					return ("ogg");
				}
			}
			return false;
		}

		// vedio播放时触发，绘制vedio帧图像到canvas  
		//        video.addEventListener('play', function () {  
		//            drawVideoAtCanvas(video, context2);  
		//        }, false);  

		//拍照  
		function getPhoto() {
			context1.drawImage(video, 0, 0, 374, 280); //将video对象内指定的区域捕捉绘制到画布上指定的区域，实现拍照。  

			var imgData = document.getElementById("canvas1").toDataURL(
					"image/png");

			// 上传到后台。  
			$.ajax({
				type : "post",
				url : "/authUserFaceImg",
				data : {
					uploadImg : imgData,
					uid : window.location.href.substring(window.location.href
							.lastIndexOf('?') + 5)
				},
				async : false,
				success : function(data) {
					if (data == "noneFaceImg") {
						alert("您的账户没有采集人像，采集后即可登录哦");
						window.location.href = "/login.jsp";
					}
					var n = window.location.href.lastIndexOf('?') + 1;
					var obj = JSON.parse(data);
					if (obj.error_code != 0 || obj.result.score <= 85) {
						alert("您的用户名和面幅不符哦，您可以重新拍照或使用用户名密码登录的哦");
					}

					if (obj.result.score > 85) {
						alert("认证成功，正在跳转您的主页...");
						window.location.href = "/loginedThroughFaceImg?"
								+ window.location.href.substring(n);
					}

				},
				error : function(data) {
					alert(data); //alert错误信息   
				}
			});
		}

		//视频  
		function getVedio() {
			drawVideoAtCanvas(video, context2);
		}

		// Converts canvas to an image
		function convertCanvasToImage(canvas) {
			var image = new Image();
			image.src = canvas.toDataURL("image/png");
			return image;
		}

		function save() {
			img = convertCanvasToImage(document.getElementById('canvas1'));
			var imgEle = docment.getElementById('img');
			imgEle.src = img;
		}
	</script>
</body>

</html>