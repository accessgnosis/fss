<%@page import="cn.xysfxy.fss.vo.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="Shortcut Icon" href="/img/Gnosisicon.png">
<link href="/user/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/user/css/bootstrap-theme.min.css" />
<link href="/css/notice2.css" rel="stylesheet" />
<link href="/css/style.css" rel="stylesheet" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<script src="/user/js/jquery-1.11.2.min.js"></script>
<script src="/js/index.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/user/js/jquery.validate.js"></script>
<script src="/user/js/jquery.validate.min.js"></script>
<script src="/user/js/bootstrap.min.js"></script>
</head>

<body>
	<div id="in-nav">
		<div class="logo">
			<a id="logo" href="goindex"> <img src="/img/LOGO.png" style="position: absolute; top: 3px;" />
			</a>
		</div>
		<div class="container1">
			<div class="row1">
				<div class="span1" style="display: inline;">
					<ul class="pull-left">
						<li class="active"><a href="goindex">网盘</a></li>
						<li><a href="findAll">分享</a></li>
					</ul>
				</div>
				<div class="span2">
					<ul class="right">
						<li class="chos" style="width: 220px;">
							<div class="sev">
								<div class="admin">
									<img src="showphoto?uid=${sessionScope.user.id }" width="30px" style="border-radius: 30px; position: absolute; top: 10px;" /> <a href="#" style="display: inline-block; height: 50px;">
										<div class="username">${sessionScope.user.name }</div> <img src="/img/VIP1.png" style="display: inline-block; margin-bottom: 45px;" />
									</a> <em style="display: inline-block;"><img src="/img/downchoose.png" class="c" style="margin-bottom: 40px;" /></em>
								</div>
								<div class="user">
									<span id="triangle-up"> </span>
									<div class="userpan">
										<div class="userpan_2">
											<div class="u1">
												<a href="#"> <img src="showphoto?uid=${sessionScope.user.id }" width="30px" class="userpic" /> <a href="">${sessionScope.user.name }</a> <img src="/img/VIP1.png" style="display: inline; margin-bottom: 5px;" />
												</a>
											</div>
										</div>
										<%
											User user = (User) session.getAttribute("user");
											if (user.getIsadmin() == 0) {
										%>
										<div class="userpan_3">
											<span> <a href="adminRedirctlogin" style="color: #000; font-size: 12px;">系统后台管理 ： </a>
												<div class="svip">管理后台</div>
											</span>
										</div>
										<%
											}
										%>
										<div class="userpan_4">
											<p>
												<a href="per-center"><span>个人资料</span></a>
											</p>
											<p>
												<a href="logout"><span>退出</span></a>
											</p>
										</div>
									</div>
								</div>
							</div>

						</li>
						<li><a href="noticeList"><img src="/img/notice.png" style="margin-top: 10px;" /></a>
							<p class="new-notice"></p></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="clean" style="clear: all; height: 40px;"></div>
	<div class="noticePage">
		<div style="height: 50px;">
			<a href="noticeList" class="goback">返回</a>
		</div>
		<div class="noticeTitle">${param.title}</div>
		<div class="noticeContent">
			<textarea style="width: 700px; height: 400px">${param.text}</textarea>
		</div>
	</div>

</body>
</html>
