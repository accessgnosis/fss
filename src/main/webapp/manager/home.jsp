<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Mobile specific metas -->
<meta name="author" content="SuggeElson" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="application-name" content="sprFlat admin template" />
<!-- Import google fonts - Heading first/ text second -->
<link href="assets/css/icons.css" rel="stylesheet" />
<!-- jQueryUI -->
<link href="assets/css/sprflat-theme/jquery.ui.all.css" rel="stylesheet" />
<!-- Bootstrap stylesheets (included template modifications) -->
<link href="assets/css/bootstrap.css" rel="stylesheet" />
<!-- Plugins stylesheets (all plugin custom css) -->
<link href="assets/css/plugins.css" rel="stylesheet" />
<!-- Main stylesheets (template main css file) -->
<link href="assets/css/main.css" rel="stylesheet" />
<!-- Custom stylesheets ( Put your own changes here ) -->
<link href="assets/css/custom.css" rel="stylesheet" />
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/img/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png">
<link rel="icon" href="assets/img/ico/favicon.ico" type="image/png">
<!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
<meta name="msapplication-TileColor" content="#3399cc" />
<link href="/manager/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/manager/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="/manager/css/home.css" />
<script type="text/javascript" src="/manager/js/jquery-3.2.1.min.js"></script>

<script src="/manager/js/jquery.validate.js"></script>
<script src="/manager/js/jquery.validate.min.js"></script>
<script src="/manager/js/messages_zh.js"></script>
<script type="text/javascript" src="/manager/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/manager/js/home.js"></script>
<style>
.error {
	padding-left: 15px;
	padding-left: 15px;
	color: red;
}
</style>
<script type="text/javascript">
	function logout() {
		if (confirm("确认退出？")) {
			window.location.href = "goindex";
		}
	}
</script>
<title>管理界面 - 师院文件共享管理系统</title>
</head>
<body>
	<!-- Start #header -->
	<div id="header">
		<div class="container-fluid">
			<div class="navbar">
				<div class="navbar-header">
					<a class="navbar-brand" href="/manager/adminlogin"> <i class="im-windows8 text-logo-element animated bounceIn"></i><span class="text-logo">管理</span><span class="text-slogan">后台</span>
					</a>
				</div>
				<nav class="top-nav" role="navigation">
				<ul class="nav navbar-nav pull-right">
					<li class="dropdown"><a href="#" data-toggle="dropdown"> <img class="user-avatar" src="showphoto?uid=${sessionScope.user.id }" alt="SuggeElson">${sessionScope.user.name }</a>
						<ul class="dropdown-menu right" role="menu">
							<li><a data-toggle="modal" data-target="#editPwd"><i class="st-settings"></i> 修改密码</a></li>
							<li><a href="logout()"><i class="im-exit"></i> 退出</a></li>
						</ul></li>
				</ul>
				</nav>
			</div>
		</div>
		<!-- Start .header-inner -->
	</div>




	<div id="sidebar">
		<div class="sidebar-inner">
			<ul id="sideNav" class="nav nav-pills nav-stacked">
				<li><a id="getFrist">主页面 <i class="im-screen"></i></a></li>
				<li><a id="getUserAll">用户管理 <i class="st-chart"></i></a></li>
				<li><a id="getUserfile"><i class="en-upload"></i> 文件管理</a></li>
				<li><a id="getNotice"><i class="st-diamond"></i> 网站公告管理 </a></li>
			</ul>
		</div>
		<script type="text/javascript">
			$('#getUserAll').click(function() {
				$('#page').attr("src", "getUserAll");
			});
			$('#getUserfile').click(function() {
				$('#page').attr("src", "getUserfile");
			});
			$('#getShare').click(function() {
				$('#page').attr("src", "getShare");
			});
			$('#getNotice').click(function() {
				$('#page').attr("src", "getNotice");
			});
			$('#getFrist').click(function() {
				$('#page').attr("src", "getFrist");
			});
		</script>

		<!-- 
		<div class="cont_right">
			<iframe src="/manager/frist.jsp" id="page" class="page" scrolling="no" name="content"></iframe>
		</div>
		 -->

		<div id="content" class="cont_right">
			<iframe src="/getFrist" id="page" class="page" scrolling="no" name="content"></iframe>
		</div>
		<!-- End .page-header -->

	</div>




	<div class="modal fade" id="editPwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">修改密码</h4>
				</div>
				<form action="updatePassAction" id="updatePassForm" method="post">
					<ol class="right-03">
						<li class="pwd-li1"><span>密码:</span><input type="password" id="u_pwd1" value="" name="passWord"></li>
						<li class="pwd-li2"><span>确认密码:</span><input type="password" id="u_pwd2" value="" name="repassword"></li>
				</form>
				<br />
				<div class="modal-footer" class="update_btn">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary">修改</button>
				</div>
			</div>
		</div>
	</div>




	<script type="text/javascript">
		$(document).ready(function() {
			$("#updatePassForm").validate({
				rules : {
					passWord : {
						required : true,
						rangelength : [ 6, 14 ]
					},
					repassword : {
						required : true,
						rangelength : [ 6, 14 ],
						equalTo : "#u_pwd1"
					}
				},
				messages : {
					passWord : {
						required : "请输入密码",
						rangelength : "密码长度为6-14个字符"
					},
					repassword : {
						required : "请输入密码",
						rangelength : "密码长度为6-14个字符",
						equalTo : "两次密码不一致"
					}
				}

			});
		})
	</script>
</body>
</html>