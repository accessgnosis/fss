package cn.xysfxy.fss.service;

import java.util.List;
import java.util.Map;

import cn.xysfxy.fss.vo.Friend;
import cn.xysfxy.fss.vo.User;

public interface IFriendService {

	List<User> getAll(String uid);

	User findOne(String friendname);

	void insertfriend(Friend friend);

	List<User> getfriAll(String id);

	List<Friend> getFriList(String id);

	User getUser(String fid);

	Friend findfrione(Friend friend);

	void deleteOne(Friend friend);

	void addOne(Friend friend);
}
