package cn.xysfxy.fss.service.impl;

import java.util.List;

import cn.xysfxy.fss.dao.FriendDao;
import cn.xysfxy.fss.service.IFriendService;
import cn.xysfxy.fss.vo.Friend;
import cn.xysfxy.fss.vo.User;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FriendServiceImpl implements IFriendService {

	private FriendDao friendDao;

	public void save(Friend p0) {
		// TODO Auto-generated method stub

	}

	public void update(Friend p0) {
		// TODO Auto-generated method stub

	}

	public boolean delete(String p0) {
		// TODO Auto-generated method stub
		return false;
	}

	public Friend get(String p0) {
		// TODO Auto-generated method stub
		return null;
	}

	/* 好友列表 */
	public List<User> getAll(String uid) {
		// TODO Auto-generated method stub

		return friendDao.findAll(uid);
	}

	/* 查询好友 */
	public User findOne(String friendname) {
		// TODO Auto-generated method stub
		return friendDao.findOne(friendname);
	}

	public void insertfriend(Friend friend) {
		// TODO Auto-generated method stub
		this.friendDao.addfriend(friend);
	}

	public List<User> getfriAll(String uid) {
		// TODO Auto-generated method stub
		return friendDao.findfriAll(uid);
	}

	public List<Friend> getFriList(String id) {
		return this.friendDao.getFriList(id);
	}

	public void save(User p0) {
		// TODO Auto-generated method stub

	}

	public void update(User p0) {
		// TODO Auto-generated method stub

	}

	public User getUser(String fid) {
		return this.friendDao.getUser(fid);
	}

	public Friend findfrione(Friend friend) {
		// TODO Auto-generated method stub
		return this.friendDao.getfirone(friend);
	}

	public void deleteOne(Friend friend) {
		// TODO Auto-generated method stub
		this.friendDao.deleteOne(friend);
	}

	public void addOne(Friend friend) {
		// TODO Auto-generated method stub
		this.friendDao.addOne(friend);
	}

}
