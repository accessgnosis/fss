package cn.xysfxy.fss.service.impl;

import java.util.List;

import cn.xysfxy.fss.dao.NoticeDao;
import cn.xysfxy.fss.service.INoticeService;
import cn.xysfxy.fss.vo.Notice;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NoticeServiceImpl implements INoticeService {
	private NoticeDao noticeDao;

	/* 插入消息公告 */
	public void save(Notice notice) {
		noticeDao.insert(notice);
	}

	/* 查找消息公告 */
	public List<Notice> getAll() {
		List<Notice> list = noticeDao.getAll();
		return list;
	}

	/* 删除消息公告 */
	public void delete(int id) {
		noticeDao.delete(id);
	}

	public int sum() {
		return noticeDao.sum();
	}

	/* 分页 */
	public List<Notice> page(int beginRow, int pageSize) {
		return noticeDao.page(beginRow, pageSize);
	}

	public void update(Notice p0) {
		// TODO Auto-generated method stub

	}

	public boolean delete(String p0) {
		return false;
		// TODO Auto-generated method stub

	}

	public Notice get(String p0) {
		// TODO Auto-generated method stub
		return null;
	}

}
