package cn.xysfxy.fss.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmailUtil {
	public static String createRandomVcode() {
		// 验证码
		String vcode = "";
		for (int i = 0; i < 6; i++) {
			vcode = vcode + (int) (Math.random() * 9);
		}
		return vcode;
	}

	public static MimeMessage createSimpleMail(Session session, String email, String code) throws Exception {
		// 创建邮件对象
		MimeMessage mm = new MimeMessage(session);
		// 设置发件人
		mm.setFrom(new InternetAddress("245034447@qq.com"));
		// 设置收件人
		mm.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
		// 设置抄送人

		String MailBody ="<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"<meta charset=\"UTF-8\">\r\n" + 
				"<!-- 最新版本的 Bootstrap 核心 CSS 文件 -->\r\n" + 
				"<link rel=\"stylesheet\" href=\"https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\r\n" + 
				"\r\n" + 
				"<!-- 可选的 Bootstrap 主题文件（一般不用引入） -->\r\n" + 
				"<link rel=\"stylesheet\" href=\"https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\" crossorigin=\"anonymous\">\r\n" + 
				"\r\n" + 
				"<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->\r\n" + 
				"<script src=\"https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>\r\n" + 
				"<title>发送给用户的验证码邮件</title>\r\n" + 
				"</head>\r\n" + 
				"<body>\r\n" + 
				"	<div class=\"container-fluid\">\r\n" + 
				"		<div class=\"row-fluid\">\r\n" + 
				"			<div class=\"span12\">\r\n" + 
				"				<div class=\"row-fluid\">\r\n" + 
				"					<div class=\"span2\"></div>\r\n" + 
				"					<div class=\"span8\">\r\n" + 
				"						<h3 class=\"text-center\">\r\n" + 
				"							<strong>【师院文件共享系统】官方邮件</strong>\r\n" + 
				"						</h3>\r\n" + 
				"						<h3>\r\n" + 
				"							<span>尊敬的用户,您好！</span>\r\n" + 
				"						</h3>\r\n" + 
				"						<p>\r\n" + 
				"							<span>这封信是由 【师院文件共享系统】 发送的。</span><br /> <span>您收到这封邮件，是由于在 【师院文件共享系统】 获取了新用户注册地址使用 了这个邮箱地址。</span><br /> <span>如果您并没有访问过【师院文件共享系统】，或没有进行上述操作，请忽略这封邮件。您不需要退订或进行其他进一步的操作。</span>\r\n" + 
				"						</p>\r\n" + 
				"						<h3 class=\"text-center text-warning\">\r\n" + 
				"							<strong><span>您的验证码为</span></strong>\r\n" + 
				"						</h3>\r\n" + 
				"						<div class=\"alert alert-error text-center\">\r\n" + 
				"							<h1 style=\"color: RED\"> "+ code +"</h1>\r\n" + 
				"							<strong>警告!</strong> 请注意你的个人隐私安全.\r\n" + 
				"						</div>\r\n" + 
				"						<p class=\"text-center\">\r\n" + 
				"							<span>有效期为180秒</span>\r\n" + 
				"						</p>\r\n" + 
				"						<blockquote class=\"text-center\">\r\n" + 
				"							<p>感谢您的访问，祝您使用愉快！如有疑虑请详询17629262609（客服电话）</p>\r\n" + 
				"							<small>【师院文件共享系统】</small>\r\n" + 
				"						</blockquote>\r\n" + 
				"					</div>\r\n" + 
				"					<div class=\"span2\"></div>\r\n" + 
				"				</div>\r\n" + 
				"			</div>\r\n" + 
				"		</div>\r\n" + 
				"	</div>\r\n" + 
				"</body>\r\n" + 
				"</html>";
		mm.setSubject("【师院文件共享系统】官方邮件");
		mm.setContent(MailBody, "text/html;charset=gbk");

		return mm;

	}

	public static void sendEmail(String email, String code) throws Exception {
		Properties prop = new Properties();
		prop.put("mail.host", "smtp.qq.com");
		prop.put("mail.transport.protocol", "smtp");
		prop.put("mail.smtp.auth", true);
		prop.put("mail.smtp.port", "587");
		// 使用java发送邮件5步骤
		// 1.创建sesssion
		Session session = Session.getInstance(prop);
		// 开启session的调试模式，可以查看当前邮件发送状态
		session.setDebug(true);

		// 2.通过session获取Transport对象（发送邮件的核心API）
		Transport ts = session.getTransport();

		// 3.通过邮件用户名密码链接
		ts.connect("245034447@qq.com", "dyxjbuuzvlxecahj");

		// 4.创建邮件
		Message msg = createSimpleMail(session, email, code);

		// 5.发送电子邮件
		ts.sendMessage(msg, msg.getAllRecipients());

	}

	public static void main(String[] args) throws Exception {
		SendEmailUtil.sendEmail("815540606@qq.com", SendEmailUtil.createRandomVcode());
	}

}
