package cn.xysfxy.fss.web;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

import org.apache.struts2.ServletActionContext;

import cn.xysfxy.fss.service.*;
import cn.xysfxy.fss.vo.Notice;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.opensymphony.xwork2.ActionSupport;

@Setter
@Getter
public class NoticeAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private Notice notice;
	private INoticeService iNoticeService;
	private int beginRow;
	private int pageSize = 5;
	private int pageSum;
	private int currentPage;
	private Map<String, Object> map;

	public static long getSerialversionuid() {
		return NoticeAction.serialVersionUID;
	}

	/* 插入消息公告 */
	public String save() {
		iNoticeService.save(notice);
		return "success";
	}

	/* 删除消息公告 */
	public String delete() {
		map = new HashMap<String, Object>();
		HttpServletRequest request = ServletActionContext.getRequest();
		String[] s = request.getParameterValues("checkbox");
		int[] a = new int[s.length];
		for (int i = 0; i < s.length; i++) {
			a[i] = Integer.parseInt(s[i]);
			iNoticeService.delete(a[i]);
		}
		return SUCCESS;

	}

	/* 查找消息公告 */
	public String getAll() {
		HttpServletRequest request = ServletActionContext.getRequest();
		List<Notice> notice = iNoticeService.getAll();
		request.setAttribute("notice", notice);
		return "success";
	}

	/* 消息公告详情 */
	public String noticeDetail() {
		return "success";
	}

	/* 公告数量 */
	public String sum() {
		map = new HashMap<String, Object>();
		int notice = iNoticeService.sum();
		map.put("allNotice", notice);
		return "json";

	}

	/* 分页 */
	public String page() {
		HttpServletRequest request = ServletActionContext.getRequest();
		Notice notice2 = new Notice();
		int sum = iNoticeService.sum();
		int i = (int) Math.ceil(sum / 5.0);
		if (currentPage == 0) {
			currentPage = 1;
		} else if (currentPage >= 1 && currentPage < i) {
			currentPage = currentPage;
		} else {
			currentPage = i;
		}
		notice2.setNowPage(currentPage);
		notice2.setSumPage(i);
		beginRow = (currentPage - 1) * pageSize;
		List<Notice> notice = iNoticeService.page(beginRow, pageSize);
		request.setAttribute("notice", notice);
		request.setAttribute("notice2", notice2);
		return "success";

	}

	public String more() {
		return "success";
	}
}
