package cn.xysfxy.fss.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.http.HttpSession;

import cn.xysfxy.fss.service.IUserService;
import cn.xysfxy.fss.util.CommonUtil;
import cn.xysfxy.fss.vo.User;
import lombok.Getter;
import lombok.Setter;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@Setter
@Getter
public class UserRegisterAction extends ActionSupport implements ModelDriven<User> {
	private static final long serialVersionUID = 1L;
	private User user = new User();
	private String massger;
	private IUserService iUserService;
	ActionContext actionContext = ActionContext.getContext();
	HttpSession session = CommonUtil.createSession();

	public User getModel() {
		return user;
	}

	/* 用户注册 */
	public String register() throws Exception {
		String id = CommonUtil.createUUID();
		String password = CommonUtil.getMD5(user.getPassWord());
		user.setPassWord(password);
		user.setId(id);
		File file = new File("D:\\Pictures\\头像\\495584989b4889bce256a6c7c2f14ae2-img2ico.net.jpg");
		InputStream in = new FileInputStream(file);
		byte[] photo = new byte[in.available()];
		in.read(photo);
		in.close();
		user.setPhoto(photo);
		this.iUserService.save(user);
		this.setMassger("注册成功，请重新登录！");
		return SUCCESS;
	}

}
