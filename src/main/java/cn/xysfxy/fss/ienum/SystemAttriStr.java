package cn.xysfxy.fss.ienum;

import java.io.File;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SystemAttriStr {
	private String SYSTEM_FOLDER_SAVE_PATH = new String();
	private String FACE_IMG_SAVE_PATH = new String();

	public SystemAttriStr() {
		if (System.getProperty("os.name").contains("Windows")) {
			SYSTEM_FOLDER_SAVE_PATH = "D:" + File.separatorChar + "FssFile" + File.separatorChar;
		} else {
			SYSTEM_FOLDER_SAVE_PATH = File.separatorChar + "home" + File.separatorChar + System.getProperty("user.name")
					+ File.separatorChar + "FssFile" + File.separatorChar;
		}
	}

	public static String getFACE_IMG_SAVE_PATH() {
		if (System.getProperty("os.name").contains("Windows")) {
			return "D:" + File.separatorChar + "FssFile" + File.separatorChar + "faceImg" + File.separatorChar;
		} else {
			return File.separatorChar + "home" + File.separatorChar + System.getProperty("user.name")
					+ File.separatorChar + "FssFile" + File.separatorChar + "faceImg" + File.separatorChar;
		}
	}

	public static void main(String[] args) {
		// System.out.println(System.getProperty("user.name"));
		// System.out.println(System.getProperty("os.name"));
		System.out.println(new SystemAttriStr().getSYSTEM_FOLDER_SAVE_PATH());

	}
}
