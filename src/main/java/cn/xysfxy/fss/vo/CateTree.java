package cn.xysfxy.fss.vo;

import java.sql.Timestamp;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class CateTree {
	private String cateid;
	private String catename;
	private List<CateTree> list;
}
