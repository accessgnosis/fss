package cn.xysfxy.fss.vo;

import java.sql.Timestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Categorie {
	private String id;
	private String name;
	private String reid;
	private String uid;
	private Integer state;
	private Timestamp addtime;
}
