package cn.xysfxy.fss.vo;

import java.sql.Timestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Share {
	private String id;
	private String uid;
	private String magid;
	private String pwd;
	private Timestamp startTime;
	private Integer retain;
}
