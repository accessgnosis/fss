package cn.xysfxy.fss.vo;

import java.sql.Timestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Sharefile {
	private String id;
	private String magid;
	private String fileandcateid;
	private int iscate;
}
