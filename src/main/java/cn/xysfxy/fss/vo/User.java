package cn.xysfxy.fss.vo;

import java.sql.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class User
{
    private String id;
    private String name;
    private String passWord;
    private String trueName;
    private String email;
    private String phone;
    private Integer isadmin;
    private Integer sex;
    private String about;
    private Timestamp addtime;
    private byte[] photo;
    private byte[] userFaceImg;
}
