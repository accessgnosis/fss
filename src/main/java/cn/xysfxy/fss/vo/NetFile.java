package cn.xysfxy.fss.vo;

import java.sql.Timestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class NetFile {
	private String id;
	private String name;
	private String uid;
	private String catid;
	private String path;
	private Timestamp addtime;
	private String type;
	private Long size;
	private Integer downum;
	private Integer deletesign;
}
