package cn.xysfxy.fss.vo;

import java.sql.Timestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class State {
	private long readedBytes = 0L;
	private long totalBytes = 0L;
	private int currentItem = 0;
	private int rate = 0;
}