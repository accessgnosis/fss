package cn.xysfxy.fss.vo;

import java.sql.Timestamp;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Notice {
	private Integer id;
	private String title;
	private String text;
	private Timestamp time;
	private int sumPage;
	private int nowPage;
}
