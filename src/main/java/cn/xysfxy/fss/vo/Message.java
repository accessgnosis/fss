package cn.xysfxy.fss.vo;

import java.sql.Timestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Message {
	private String id;
	private String userid;
	private String friendid;
	private String content;
	private Timestamp sendtime;
	private int msg_static;
	private int type;
}
